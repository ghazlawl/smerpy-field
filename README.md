# Smerpy Field

This is a sandbox custom field type for Drupal 8. This module was created as a 
test to understand how custom field types, widgets, and formatters work in 
Drupal 8.

I used the [`Google Map Field`](https://www.drupal.org/project/google_map_field) 
module from Drupal.org and the core [`StringTextfieldWidget`](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Field%21Plugin%21Field%21FieldWidget%21StringTextfieldWidget.php/class/StringTextfieldWidget/8.2.x) 
class in Drupal as guides for this module.

**This module shows you how to:**

* Create a custom field type (similar to textfield, textarea, date, etc).
* Create a custom field widget (the form element/s that are displayed when editing an entity with our field on it). 
* Create a custom field formatter (to provide a template file so the output can be overridden).
* Create a custom settings form for our field widget (so the user can specify placeholder text, etc).
