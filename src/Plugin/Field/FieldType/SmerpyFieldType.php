<?php

namespace Drupal\smerpy_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * @FieldType(
 *   id = "smerpy_field",
 *   label = @Translation("Smerpy"),
 *   description = @Translation("This field stores Smerpy fields in the database."),
 *   default_widget = "smerpy_field_default",
 *   default_formatter = "smerpy_field_default"
 * )
 */
class SmerpyFieldType extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'name' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ],
        'color' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['name'] = DataDefinition::create('string')
      ->setLabel(t('Name'));

    $properties['color'] = DataDefinition::create('string')
      ->setLabel(t('color'));

    return $properties;
  }
}
