<?php

namespace Drupal\smerpy_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldWidget(
 *   id = "smerpy_field_default",
 *   label = @Translation("Smerpy Field Default"),
 *   field_types = {
 *     "smerpy_field"
 *   }
 * )
 */
class SmerpyFieldDefaultWidget extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'size' => 60,
      'placeholder' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['size'] = [
      '#type' => 'number',
      '#title' => $this->t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];

    $element['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Textfield size: @size', array('@size' => $this->getSetting('size')));
    $placeholder = $this->getSetting('placeholder');

    if (!empty($placeholder)) {
      $summary[] = $this->t('Placeholder: @placeholder', array('@placeholder' => $placeholder));
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Wrap our elements in a fieldset.
    $element += [
      '#type' => 'fieldset',
      '#title' => t('Smerpy'),
    ];

    // Add the name field.
    $element['name'] = [
      '#title' => t('Name'),
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->name) ? $items[$delta]->name : '',
      '#placeholder' => $this->getSetting('placeholder'),
      '#size' => $this->getSetting('size'),
    ];

    // Add the color field.
    $element['color'] = [
      '#title' => t('Color'),
      '#type' => 'select',
      '#options' => [
        '' => $this->t('Select'),
        'orange' => $this->t('Orange'),
        'white' => $this->t('White'),
        'red' => $this->t('Red'),
        'blue' => $this->t('Blue'),
        'green' => $this->t('Green'),
      ],
      '#default_value' => isset($items[$delta]->color) ? $items[$delta]->color : '',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Hold the valid values.
    $valid_values = [];

    foreach ($values as &$value) {
      if (trim($value['name']) != '' || trim($value['color']) != '') {
        // If we have a value in either field, then it's valid and should be added.
        $valid_values[] = $value;
      }
    }

    return $valid_values;
  }
}
