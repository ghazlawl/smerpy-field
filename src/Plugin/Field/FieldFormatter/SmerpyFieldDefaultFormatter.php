<?php

namespace Drupal\smerpy_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Component\Utility\SafeMarkup;

/**
 * @FieldFormatter(
 *   id = "smerpy_field_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "smerpy_field"
 *   }
 * )
 */
class SmerpyFieldDefaultFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $element = [
        '#theme' => 'smerpy_field',
        '#name' => SafeMarkup::checkPlain($item->name),
        '#color' => SafeMarkup::checkPlain($item->color),
      ];
      $elements[$delta] = [
        '#markup' => drupal_render($element)
      ];
    }

    return $elements;
  }
}
